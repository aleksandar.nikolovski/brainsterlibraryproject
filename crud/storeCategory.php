<?php

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    require_once __DIR__ . "/../conn.php";

    $sql = "INSERT INTO categories (category, is_deleted) VALUES (:category, :is_deleted)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['category' => $_POST['category'], 'is_deleted' => $_POST['is_deleted']]);

    header("Location: createCategory.php");
} else {
    header("Location: ./../index.php");
    die();
}
