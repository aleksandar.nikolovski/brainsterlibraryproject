$(document).ready(function () {
  $(".delete").click(function (e) {
    console.log(e.currentTarget.id);
    Swal.fire({
      title: "Are you sure you want to delete this book?",
      text: "- By deleting it all the comments and notes from the users are going to be deleted aswell",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete the book!",
    }).then((result) => {
      if (result.value) {
        let bookId = e.currentTarget.id;

        $.ajax({
          type: "POST",
          url: "./deleteBook.php",
          data: {
            bookId: bookId,
          },
          success: function (data) {
            result = $.parseJSON(data);

            console.log(result.status);
            if (result.status == "success") {
              Swal.fire("Deleted!", "The book has been deleted.", "success");
              $('button[id="' + bookId + '"]').hide();
            }

            if (result.status == "error") {
              Swal.fire(
                "Error!",
                "There was an error. The book was not deleted!",
                "error"
              );
            }
          },
          error: function () {
            Swal.fire("Error!", "There was a DB connection error!.", "error");
          },
        });
      }
    });
  });
});
