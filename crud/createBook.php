<?php
require_once __DIR__ . "/../functions.php";
session_init();

if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 1) {

        require_once __DIR__ . "/../conn.php";

        $authors = "SELECT * FROM authors
        WHERE is_deleted = 0";
        $stmtAuthors = $pdo->prepare($authors);
        $stmtAuthors->execute();


        $categories = "SELECT * FROM categories
        WHERE is_deleted = 0";
        $stmtCategories = $pdo->prepare($categories);
        $stmtCategories->execute();


        $sqlAll = "SELECT books.*, authors.name as authorName, authors.surname as authorSurname , categories.category as bookCategory FROM books
        JOIN authors ON authors.id = books.author_id
        JOIN categories ON categories.id = books.category_id";
        $stmtAll = $pdo->prepare($sqlAll);
        $stmtAll->execute();
    }
} else {
    header("Location: ./../index.php");
    die();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Project2</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous" />

</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-4 offset-3">
                <h1>Add a new Book</h1>

                <form action="./storeBook.php" method="POST" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control <?php if (isset($_SESSION['title'])) echo 'is-invalid' ?>" id="title" name="title" aria-describedby="emailHelp" placeholder="Enter title">

                        <?php if (isset($_SESSION['title'])) { ?>
                            <div class="invalid-feedback">
                                <?= $_SESSION['title'] ?>
                            </div>
                        <?php }
                        unset($_SESSION['title']); ?>
                    </div>

                    <div class="form-group">
                        <label for="author">Select Author</label>
                        <select class="form-control <?php if (isset($_SESSION['author'])) echo 'is-invalid' ?>" name="author" id="author">
                            <option selected disabled value="" class="">Choose..</option>
                            <?php while ($authors = $stmtAuthors->fetch()) { ?>
                                <option value="<?= $authors['id'] ?>"><?php echo $authors['name'];
                                                                        echo $authors['surname'] ?></option>
                            <?php } ?>
                        </select>

                        <?php if (isset($_SESSION['author'])) { ?>
                            <div class="invalid-feedback">
                                <?= $_SESSION['author'] ?>
                            </div>
                        <?php }
                        unset($_SESSION['author']); ?>
                    </div>

                    <div class="form-group">
                        <label for="year">Year Published</label>
                        <input type="number" class="form-control <?php if (isset($_SESSION['year'])) echo 'is-invalid' ?>" id="year" name="year" placeholder="Enter Year">

                        <?php if (isset($_SESSION['year'])) { ?>
                            <div class="invalid-feedback">
                                <?= $_SESSION['year'] ?>
                            </div>
                        <?php }
                        unset($_SESSION['year']); ?>
                    </div>

                    <div class="form-group">
                        <label for="pages">Pages</label>
                        <input type="number" class="form-control <?php if (isset($_SESSION['pages'])) echo 'is-invalid' ?>" id="pages" name="pages" placeholder="Enter Pages">

                        <?php if (isset($_SESSION['pages'])) { ?>
                            <div class="invalid-feedback">
                                <?= $_SESSION['pages'] ?>
                            </div>
                        <?php }
                        unset($_SESSION['pages']); ?>
                    </div>

                    <div class="form-group">
                        <label for="picture">picture URL</label>
                        <input type="text" class="form-control <?php if (isset($_SESSION['picture'])) echo 'is-invalid' ?>" id="picture" name="picture" placeholder="Enter URL">

                        <?php if (isset($_SESSION['picture'])) { ?>
                            <div class="invalid-feedback">
                                <?= $_SESSION['picture'] ?>
                            </div>
                        <?php }
                        unset($_SESSION['picture']); ?>
                    </div>

                    <div class="form-group">
                        <label for="category">Select Category</label>
                        <select class="form-control <?php if (isset($_SESSION['category'])) echo 'is-invalid' ?>" name="category" id="category">
                            <option selected disabled value="">Choose..</option>
                            <?php while ($categories = $stmtCategories->fetch()) { ?>
                                <option value="<?= $categories['id'] ?>"><?= $categories['category'] ?></option>
                            <?php } ?>
                        </select>

                        <?php if (isset($_SESSION['category'])) { ?>
                            <div class="invalid-feedback">
                                <?= $_SESSION['category'] ?>
                            </div>
                        <?php }
                        unset($_SESSION['category']); ?>
                    </div>

                    <button type="submit" class="btn btn-success">Store Book</button>
                </form>
                <div>
                    <a class="btn btn-danger mt-3" href="./../index.php">Back</a>
                </div>
            </div>



            <div class="col-12">
                <div class="row">
                    <div class="col-10">
                        <h1>All Books</h1>
                    </div>

                    <div class="col-10">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">author</th>
                                    <th scope="col">year</th>
                                    <th scope="col">pages</th>
                                    <th scope="col">picture</th>
                                    <th scope="col">category</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php while ($books = $stmtAll->fetch()) { ?>
                                    <tr>
                                        <th><?= $books['id'] ?></th>
                                        <td> <?= $books['title'] ?></td>
                                        <td> <?php echo $books['authorName'];
                                                echo "<br>";
                                                echo $books['authorSurname'] ?></td>
                                        <td> <?= $books['year'] ?></td>
                                        <td> <?= $books['pages'] ?></td>
                                        <td class="text-break"> <?= $books['picture'] ?></td>
                                        <td> <?= $books['bookCategory'] ?></td>

                                        <td>
                                            <a href="editBook.php?id=<?= $books['id'] ?>" class="btn btn-warning mt-2">Edit</a>
                                            <?php if ($books['is_deleted'] == 0) { ?>
                                                <button id="<?= $books['id'] ?>" class="btn btn-danger mt-2 delete">Delete</button>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="./deleteBook.js"></script>
</body>

</html>