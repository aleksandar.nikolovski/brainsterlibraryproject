<?php
require_once __DIR__ . "/../functions.php";
session_init();

if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 1) {

        require_once __DIR__ . "/../conn.php";

        $sql = "SELECT * FROM comments WHERE approved = 1";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();


        $sqlRejected = "SELECT * FROM comments WHERE approved = 3";
        $stmtRejected = $pdo->prepare($sqlRejected);
        $stmtRejected->execute();
    }
} else {
    header("Location: ./../index.php");
    die();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Project2</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous" />

    <style>
        body {
            background-color: grey;
        }
    </style>

</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 text-center mt-5">
                        <a class="btn btn-danger mt-3" href="./../index.php">Back to main page</a>
                    </div>

                    <div class="col-4">
                        <h1>Comments for review</h1>

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col">Cooment</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php while ($rows = $stmt->fetch()) { ?>
                                    <tr>
                                        <td><?= $rows['id'] ?></td>
                                        <td> <?= $rows['comment'] ?></td>

                                        <td>
                                            <a href="approveComment.php?id=<?= $rows['id'] ?>" class="btn btn-success mt-2">Approve</a>
                                            <?php if ($rows['approved'] == 1) { ?>
                                                <a href="rejectComments.php?id=<?= $rows['id'] ?>" class="btn btn-danger mt-2">Reject</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>


                    <div class="col-4 offset-3">
                        <h1>Rejected comments</h1>
                        <table class="table table-hover table-danger">
                            <thead>
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col">Cooment</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php while ($rejected = $stmtRejected->fetch()) { ?>
                                    <tr>
                                        <td><?= $rejected['id'] ?></td>
                                        <td> <?= $rejected['comment'] ?></td>

                                        <td>
                                            <a href="approveComment.php?id=<?= $rejected['id'] ?>" class="btn btn-success mt-2">Approve</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>


        <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

</body>

</html>