<?php

require_once __DIR__ . "/../functions.php";
session_init();

if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 1) {

        require_once __DIR__ . "/../conn.php";

        $sql = "UPDATE comments SET
         approved = :approved 
         WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['id' => $_GET['id'], 'approved' => 0]);

        header("Location: ./manageComments.php");
        die();
    }
} else {
    header("Location: ./../index.php");
    die();
}
