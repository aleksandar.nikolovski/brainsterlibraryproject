<?php

require_once __DIR__ . "/../functions.php";
session_init();

if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 1) {

        require_once __DIR__ . "/../conn.php";

        $authors = "SELECT * FROM authors";
        $stmtAuthors = $pdo->prepare($authors);
        $stmtAuthors->execute();


        $categories = "SELECT * FROM categories";
        $stmtCategories = $pdo->prepare($categories);
        $stmtCategories->execute();


        $sql = "SELECT books.*, authors.name as authorName, authors.surname as authorSurname , categories.category as bookCategory FROM books
        JOIN authors ON authors.id = books.author_id
        JOIN categories ON categories.id = books.category_id
        WHERE books.id = :id
        LIMIT 1";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['id' => $_GET['id']]);

        $books = $stmt->fetch();
    }
}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Project2</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous" />

    <style>
        body {
            background-color: grey;
        }
    </style>

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-4 offset-3">
                        <h1>Edit Book</h1>
                        <form action="./updateBook.php" method="POST">
                            <input type="hidden" name="id" value="<?= $books['id'] ?>">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title" value="<?= $books['title'] ?>" aria-describedby="emailHelp" placeholder="Enter title">
                            </div>
                            <div class="form-group">
                                <label for="author">Select Author</label>
                                <select class="form-control" name="author" id="author">
                                    <option selected value="<?= $books['author_id'] ?>"> <?= $books['authorName'] ?> </option>

                                    <?php while ($authors = $stmtAuthors->fetch()) { ?>
                                        <option value="<?= $authors['id'] ?>">
                                            <?php echo $authors['name'];
                                            echo $authors['surname'] ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="year">Year Published</label>
                                <input type="text" class="form-control" id="year" name="year" value="<?= $books['year'] ?>" placeholder="Enter Year">
                            </div>

                            <div class="form-group">
                                <label for="pages">Pages</label>
                                <input type="text" class="form-control" id="pages" name="pages" value="<?= $books['pages'] ?>" placeholder="Enter Pages">
                            </div>

                            <div class="form-group">
                                <label for="picture">picture URL</label>
                                <input type="text" class="form-control" id="picture" name="picture" value="<?= $books['picture'] ?>" placeholder="Enter URL">
                            </div>

                            <div class="form-group">
                                <label for="category">Select Category</label>
                                <select class="form-control" name="category" id="category">
                                    <option value="<?= $books['category_id'] ?>"> <?= $books['bookCategory'] ?> </option>
                                    <?php while ($categories = $stmtCategories->fetch()) { ?>
                                        <option value="<?= $categories['id'] ?>"><?= $categories['category'] ?> </option>
                                    <?php } ?>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-success">Update Book</button>
                        </form>
                        <div>
                            <a class="btn btn-danger mt-3" href="./createBook.php">Back</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

</body>

</html>