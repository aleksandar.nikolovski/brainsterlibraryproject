<?php
require_once __DIR__ . "/../functions.php";
session_init();

require_once __DIR__ . "/../conn.php";

if (empty($_POST['username']) || empty($_POST['email']) || empty($_POST['password'])) {
    $_SESSION['error'] = "All fields are required";
    header("Location: ../signUp.php");
    die();
}

$role = $_POST['role'];
$username = $_POST['username'];
$email = $_POST['email'];
$password = password_hash($_POST['password'], PASSWORD_BCRYPT);

$sql = "SELECT * FROM users WHERE username = :username ";
$stmt = $pdo->prepare($sql);
$stmt->execute(['username' => $username]);

if ($stmt->rowCount() == 1) {
    $_SESSION['error'] = "That username already exists!";
    header("Location: ./../signUp.php");
    die();
} else {

    $sql = "INSERT INTO users (role_id, username, email, password) values (:role_id, :username, :email, :password) ";

    $stmt = $pdo->prepare($sql);
    $stmt->execute(['role_id' => $role, 'username' => $username, 'email' => $email, 'password' => $password]);


    header("Location: ../login.php");
    die();
}
