<?php
require_once __DIR__ . "/functions.php";

require_once 'conn.php';
session_init();

$sql = "SELECT books.*, authors.biography as biography, authors.name as authorName, authors.surname as authorSurname , categories.category as bookCategory FROM books
JOIN authors ON authors.id = books.author_id
JOIN categories ON categories.id = books.category_id
WHERE books.id = :id
LIMIT 1";
$stmt = $pdo->prepare($sql);
$stmt->execute(['id' => $_GET['id']]);
$book = $stmt->fetch();


$id = $_GET['id'];
$sql1 = "SELECT * FROM comments WHERE book_id = :book_id AND approved = 0 ";
$stmt1 = $pdo->prepare($sql1);
$stmt1->execute(['book_id' => $id]);


if (auth()) {
    $sqlLeftComment = "SELECT * FROM comments WHERE book_id = :book_id AND user_id = :user_id ";
    $stmtLeftComment = $pdo->prepare($sqlLeftComment);
    $stmtLeftComment->execute(['book_id' => $id, 'user_id' => $_SESSION['userid']]);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Project2</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous" />
    <link rel="stylesheet" href="book.css">
</head>

<body>

    <!-- navbar -->

    <?php
    require_once __DIR__ . "/layouts/navbar.php";
    ?>
    <!-- end navbar -->

    <div class="container">
        <div class="row">
            <div class="col-12 border-primary">
                <div class="col-8 offset-2 background-gradient ">
                    <div class="img-div card ml-auto mr-auto">
                        <img class="" src="<?= $book['picture'] ?>" alt="">
                    </div>

                    <div class="text-align">
                        <h1 class="text-center"><?= $book['title'] ?></h1>
                        <p class="text-right m-0"><b>Author:</b> <i><?= $book['authorName'] ?></i></p>
                        <p class="text-right m-0"><b>Category: </b><?= $book['bookCategory'] ?></p>
                        <p class="text-right m-0"><b>Pages: </b><?= $book['pages'] ?></p>
                        <p class="text-right m-0"><b>Published: </b><?= $book['year'] ?></p>

                        <div>
                            <h5 class="m-1">About the author</h5>
                            <p class="text-center font-italic font-weight-normal"><?= $book['biography'] ?></p>
                        </div>
                    </div>
                </div>

                <div class="col-8 offset-2 <?= (!isset($_SESSION['userid'])) || ($rows['user_id']) ? 'd-none' : 'd-block' ?>  ">
                    <h5>Your notes for the book:</h5>
                    <form method="POST">
                        <div class="form-group">
                            <input type="hidden" name="userid" id="noteUserid" value="<?= $_SESSION['userid'] ?>">
                            <input type="hidden" name="bookid" id="noteBookid" value="<?= $_GET['id'] ?>">

                            <div class="textarea-wrapper">
                                <textarea class="form-control" name="userNote" id="userNote" rows="3" placeholder="Leave a note..."></textarea>
                            </div>

                            <button type="submit" id="submit" class="btn btn-warning mt-2">Add a note</button>
                        </div>
                    </form>

                </div>
                <div class="col-6 offset-3" id="notes">

                </div>

                <div class="col-8 offset-2 approvedComments bg-comments mt-2">
                    <h6> Comments:</h6>
                    <?php while ($rows = $stmt1->fetch()) { ?>
                        <div class="bg-dark text-light p-2 mb-2  border-success rounded">
                            <p><?= $rows['comment'] ?></p>
                        </div>
                    <?php } ?>
                </div>

                <div class="col-8 offset-2 <?= (!isset($_SESSION['userid'])) || ($stmtLeftComment->rowCount() == 1) ? 'd-none' : 'd-block' ?> ">
                    <div id="commentArea">
                        <h5>Your comment for the book?</h5>
                        <form method="POST">

                            <div class="form-group">
                                <input type="hidden" name="commentUserid" id="commentUserid" value="<?= $_SESSION['userid'] ?>">
                                <input type="hidden" name="commentBookid" id="commentBookid" value="<?= $_GET['id'] ?>">

                                <div class="textarea-wrapper">
                                    <textarea class="form-control" name="userComment" id="userComment" rows="3" placeholder="Leave a comment..."></textarea>
                                </div>

                                <button type="submit" id="commentSubmit" class="btn btn-primary mt-2">Post a comment</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div id="watingApproval" class="col-6 offset-3 bg-light p-3  mb-5 rounded">

                </div>

            </div>
        </div>
    </div>

    <!-- footer  -->
    <?php
    require_once __DIR__ . "/layouts/footer.php";
    ?>

    <!-- end footer  -->


    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="booknotes.js"></script>
    <script src="bookcomments.js"></script>
    <script src="layouts/footerQuote.js"></script>

</body>

</html>