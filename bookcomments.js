//storing comments

$("#commentSubmit").click(function (e) {
    e.preventDefault();
    let comment = $("#userComment").val();
    let userid = $("#commentUserid").val();
    let bookid = $("#commentBookid").val();
    $.ajax({
      type: "POST",
      url: "bookcomments.php",
      data: { comment: comment, userid: userid, bookid: bookid },
      success: function (response) {
     
        $("#userComment").val("");
        $("#commentArea").hide();
        $("#watingApproval").show();

        
        $("#watingApproval")
        .append("<small class='text-danger font-weight-bold'>"  + "Your comment needs approval"  + "</small>")
        .append("<p id=" + response + " class='text-dark border shadow-lg rounded p-3'>"  + comment   + "</p>")
        .append("<button id=" + response +" class='btn btn-danger remove-comment' >Remove Comment</button>")
       
      },
      error: function () {
        alert("An error occurred");
      },
    });
  });


  //get comments

  $.ajax({
    type: "POST",
    url: "bookcomments.php",
    data: {
      action: "getComments",
      userid: $("#commentUserid").val(),
      bookid: $("#commentBookid").val(),
    },
    success: function (data) {
      result = $.parseJSON(data);
      

     if(result) {
      $("#watingApproval")
      .append("<small class='text-danger font-weight-bold'>"  + "Your comment needs approval"  + "</small>")
      .append("<p id=" + result.id + " class='text-dark border shadow-lg rounded p-3'>"  + result.comment   + "</p>")
      .append("<button id=" + result.id +" class='btn btn-danger remove-comment'>Remove Comment</button>")
     } else {
      console.log("User hasn't left any comment yet");
     }

    },
  });

  //remove comment

  $("body").on("click", ".remove-comment", function(e) {

    commentId = $(this).attr("id");

    $.ajax({
      type: "POST",
      url: "bookcomments.php",
      data: {
        action: "removeComment",
        commentId: commentId
      },
      success: function(data) {

        $("#commentArea").show();
        $("#watingApproval").hide();
        $("#watingApproval").text("");
      }
      
      
    });

  });