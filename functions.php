<?php

function session_init() {
    if(session_status() !== PHP_SESSION_ACTIVE) {
        session_start();
    }
}


function auth() {
    session_init();
    if(isset($_SESSION['username'])) {
        return true;
    }

    return false;
}


?>
