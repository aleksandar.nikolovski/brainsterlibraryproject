
 <div class="container-fluid fixed-tops">
        <div class="row">
            <div class="col-12 p-0">
                <nav class="navbar navbar-expand-lg navbar-light bg-dark">
                    <a class="navbar-brand ml-3" href="index.php"><img src="img/brainster.png" width="70" height="60" alt="Brainster Logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <?php if (isset($_SESSION['role'])) { ?>
                        <?php if ($_SESSION['role'] == 1) { ?>
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item mr-5">
                                        <a class="nav-link btn btn-info rounded-pill text-dark px-4 mr-3" href="./crud/createBook.php">Add Book</a>
                                    </li>
                                    <li class="nav-item mr-5">
                                        <a class="nav-link btn btn-info rounded-pill text-dark px-4 mr-3" href="./crud/createCategory.php">Add Category </a>
                                    </li>
                                    <li class="nav-item mr-5">
                                        <a class="nav-link btn btn-info rounded-pill text-dark px-4 mr-3" href="./crud/createAuthor.php">Add Author </a>
                                    </li>
                                    <li class="nav-item mr-5">
                                        <a class="nav-link btn btn-info rounded-pill text-dark px-4 mr-3" href="./crud/manageComments.php">Manage Comments </a>
                                    </li>
                                </ul>
                            </div>
                        <?php  } ?>
                    <?php  } ?>

                    <?php if (auth()) { ?>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link btn btn-danger rounded-pill text-dark px-4 mr-3" href="./Actions/logout.php">Logout <?= $_SESSION['username']?></a>
                                </li>
                            </ul>
                        </div>
                    <?php } else { ?>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link btn btn-warning rounded-pill text-dark px-4 mr-3" href="login.php">Login</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-warning rounded-pill text-dark px-3" href="signup.php">Register</a>
                                </li>
                            </ul>
                        </div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
