<?php

require_once 'conn.php';

$sql = "SELECT books.*, authors.name as authorName, authors.surname as authorSurname , categories.category as bookCategory FROM books
JOIN authors ON authors.id = books.author_id
JOIN categories ON categories.id = books.category_id
WHERE books.is_deleted = 0";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$post = $stmt->fetchAll();

echo json_encode($post);