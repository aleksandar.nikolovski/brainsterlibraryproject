$(document).ready(function () {

  //GET request for notes

  $.ajax({
    type: "POST",
    url: "booknotes.php",
    data: {
      action: "getNotes",
      userid: $("#noteUserid").val(),
      bookid: $("#noteBookid").val(),
    },
    success: function (data) {
      result = $.parseJSON(data);

      for (var i = 0; i < result.length; i++) {
     
        $("#notes").append("<p id=" + result[i].id + " class='text-dark shadow border rounded p-3'>" + result[i].note + "</p")
      .append("<button id=" +result[i].id +" class='edit btn btn-warning rounded-pill'>Edit</button> <button id=" +result[i].id +" class='delete btn btn-danger rounded-pill'>Delete</button>");
      }
    },
  });

  // saving notes in db

  $("#submit").click(function (e) {
    e.preventDefault();
    let note = $("#userNote").val();
    let userid = $("#noteUserid").val();
    let bookid = $("#noteBookid").val();
    $.ajax({
      type: "POST",
      url: "booknotes.php",
      data: { notes: note, userid: userid, bookid: bookid },
      success: function (response) {
        // $("#notes").append("<p>" + $("#userNote").val() + "</p>");
        
        $("#notes")
          .append("<p id=" +response +" class='text-dark shadow border rounded p-3'>" + $("#userNote").val() +"</p")
          .append("<button id=" +response +" class='edit btn btn-warning rounded-pill'>Edit</button> <button id=" +response +" class='delete btn btn-danger rounded-pill'>Delete</button>");

        $("#userNote").val("");
      },
      error: function () {
        alert("An error occurred");
      },
    });
  });

  // delete notes

  $("body").on("click", ".delete", function () {
    let noteId = $(this).attr("id");
    $.ajax({
      url: "booknotes.php",
      type: "POST",
      data: { noteId: noteId, delete: "delete" },
      success: function (data) {
        $('p[id="' + noteId + '"]').remove();
        $('button[id="' + noteId + '"]').remove();
      },
      error: function (error) {
        console.log(error);
      },
    });
  });


  //update notes

  $("body").on("click", ".edit", function () {
    
      let id =  $(this).attr("id");
      let noteText = $("#" + id).text();
      
      $('button[id="'+ id +'"]').hide();

      $("#" + id).replaceWith("<textarea id='" + id + "' class='form-control bg-light'>" + noteText + "</textarea>");
      $("#" + id).after("<button id='save_" + id + "' class='btn btn-success rounded-pill'>Save</button>");
  
      $("body").on("click", "#save_" + id, function() {

        let updatedNote = $("#" + id).val();
        $.ajax({
          url: "booknotes.php",
          type: "post",
          data: {
            id: id,
            updatedNote: updatedNote
          },
          success: function(response) {
            $("#" + id).replaceWith("<p id='" + id + "' class='text-dark border rounded p-3'>" + updatedNote + "</p>");
            $("#save_" + id).remove();
            $('button[id="'+ id +'"]').show();
          }
        });
      });
    });


});
